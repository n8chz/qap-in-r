library("e1071")
library("dplyr")
library("RUnit")

# permute both rows and columns
prc <- function(A, p) {
  n <- length(p)
  new_mat <- matrix(0, n, n)
  for (i in 1:n) {
    for (j in 1:n) {
      new_mat[i, j] = A[p[i], p[j]]
    }
  }
  new_mat
}

# objective function for matrices A and B, and permutation p
Z <- function(A, B, p) {
  sum(prc(A, p)*B)
}

random_points <- function(n) {
  matrix(runif(2*n), n, 2)
}

distance_matrix <- function(points) {
  n <- dim(points)[1]
  new_mat <- matrix(0, n, n)
  for (i in 1:(n-1)) {
    for (j in (i+1):n) {
      new_mat[i, j] = sqrt(sum((points[i]-points[j])^2))
      new_mat[j, i] = new_mat[i, j]
    }
  }
  new_mat
}

random_distance_matrix <- function(n) {
  distance_matrix(random_points(n))
}

random_flow_matrix <- function(n) {
  new_mat <- matrix(0, n, n)
  for (i in 1:(n-1)) {
    for (j in (i+1):n) {
      new_mat[i, j] = sample(0:2, 1)
      new_mat[j, i] = new_mat[i, j]
    }
  }
  new_mat
}

random_qap_problem <- function(n) {
  A <- random_distance_matrix(n)
  B <- random_flow_matrix(n)
  list(A, B) # there must be a better way
}

fitness_landscape <- function(q) {
  A <- q[[1]]
  B <- q[[2]]
  n <- dim(A)[1]
  P <- permutations(n)
  cbind(P, apply(P, 1, function(p) Z(A, B, p)))
}

global_minimum <- function(f) {
  n <- dim(f)[2]
  # h/t Joachim Schork https://statisticsglobe.com/r-find-index-of-max-and-min-value-of-vector-or-data-frame-column
  which.min(f[, n])
}

# Rows of return value comprise the 2-opt neighborhood of permutation p
swaps <- function(p) {
  n <- length(p)
  neighbors <- matrix(p, n*(n-1)/2, n, byrow=TRUE)
  row <- 1
  for (i in 1:(n-1)) {
    for (j in (i+1):n) {
      neighbors[row, i] <- p[j]
      neighbors[row, j] <- p[i]
      row <- row+1
    }
  }
  neighbors
}

# The watershed of the permutation p_i, or f[i, 1:(n-1)]
# consists of those permutations which have a descent path to p_i.
# TODO: total rewrite
watershed <- function(f, i) {
  definitely_in <- c(i)
  definitely_out <- NULL
  checked <- NULL
  unknown <- setdiff(1:(dim(f)[1]), definitely_in)
  while (length(unknown) > 0) {
    additions <- NULL
    for (p in setdiff(definitely_in, checked)) {
      s <- swaps(p)
      new <- apply(s, 1, function(swap) {
        which(apply(f, 1, function(ff) all(ff[1:6]==swap)))
      })
      definitely_in <- union(definitely_in, new)
      checked <- union(checked, s)
    }
    unknown <- setdiff(unknown, checked)
  }
  definitely_in
}
